const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const path = require('path');
require('dotenv').config();

const logger = require('./logger');
const companies = require('./routes/companies');
const users = require('./routes/users');

const { PORT } = process.env;

const avatarsDir = path.join(__dirname, '../public/avatars');

const app = express();

// base middlewares
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// routing
app.use(...companies);
app.use(users);

if (!fs.existsSync(avatarsDir)) {
  fs.mkdirSync(avatarsDir, { recursive: true });
}

app.listen(PORT, () => {
  logger.log(`Example app listening at http://localhost:${PORT}`);
});
