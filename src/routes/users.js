const express = require('express');

const { getById, getList, addItem, updateItem, patchItem, removeItem, getImg } = require('../services/users');

const routerUsers = express.Router();

const PREFIX_USERS = '/users';

routerUsers.get('/:id', async (req, res) => {
  try {
    const user = await getById(req.params.id);

    if (!user) {
      res.sendStatus(404);
      return;
    }

    res.send(user);
  } catch (error) {
    res.status(404).send(error);
  }
});

routerUsers.get('/', async (req, res) => {
  try {
    const users = await getList();

    res.send(users);
  } catch (error) {
    res.status(404).send(error);
  }
});

routerUsers.post('/', async (req, res) => {
  try {
    await addItem(req.body);

    res.sendStatus(201);
  } catch (error) {
    res.status(404).send(error);
  }
});

routerUsers.put('/:id', async (req, res, next) => {
  try {
    await updateItem(req.params.id, req.body);
    res.sendStatus(200);
  } catch (error) {
    res.status(404).send(error);
  }
});

routerUsers.patch('/:id', async (req, res) => {
  try {
    await patchItem(req.params.id, req.body);

    res.sendStatus(200);
  } catch (error) {
    res.status(404).send(error);
  }
});

routerUsers.delete('/:id', async (req, res) => {
  try {
    await removeItem(req.params.id);

    res.sendStatus(200);
  } catch (error) {
    res.status(404).send(error);
  }
});

const routerImg = express.Router();

const PREFIX_IMG = '/public/avatars';

routerImg.get('/:filename', async (req, res) => {
  try {
    const imgBuff = await getImg(req.params.filename.toString());

    res.writeHead(200, {
      'Content-Type': 'image/png',
      'Content-Length': imgBuff.length,
    });

    res.end(imgBuff);
  } catch (error) {
    res.status(404).send(error);
  }
});

const router = express.Router();
router.use(PREFIX_USERS, routerUsers);
router.use(PREFIX_IMG, routerImg);

// module.exports = [PREFIX, router];
module.exports = router;
