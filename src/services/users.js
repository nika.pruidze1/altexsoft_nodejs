const knex = require('../knex');
const path = require('path');
const fsPromises = require('fs').promises;

const { PORT } = process.env;

const avatarsDir = path.join(__dirname, '../../public/avatars');
const avatarsDir_URL = `http://localhost:${PORT}/public/avatars/`;

const USERS_TABLE = 'users';

module.exports = {
  async getById(id) {
    const item = await knex(USERS_TABLE).select('*').where({ id }).first();

    return item;
  },

  async getList() {
    const item = await knex(USERS_TABLE).select('*');

    return item;
  },

  async addItem(body) {
    if (body.avatar) {
      const mediaFormat = body.avatar.substring(body.avatar.indexOf('/') + 1, body.avatar.indexOf(';'));
      const FileName = Date.now().toString().concat(`.${mediaFormat}`);
      const FilePath = path.join(avatarsDir, FileName);

      const re = new RegExp(`data:image/${mediaFormat};base64,`);
      const base64Img = body.avatar.replace(re, '');

      await fsPromises.writeFile(FilePath, base64Img, 'base64');

      body.avatar = path.join(avatarsDir_URL, FileName);
    }

    return knex(USERS_TABLE).insert(body);
  },

  async updateItem(id, body) {
    const userObj = await knex(USERS_TABLE).select('*').where({ id }).first();
    if (userObj.avatar) {
      const oldFileName = path.basename(userObj.avatar);
      await fsPromises.unlink(path.join(avatarsDir, oldFileName));
    }
    let newURL;
    if (body.avatar) {
      const mediaFormat = body.avatar.substring(body.avatar.indexOf('/') + 1, body.avatar.indexOf(';'));
      const newFileName = Date.now().toString().concat(`.${mediaFormat}`);
      const newFilePath = path.join(avatarsDir, newFileName);

      const re = new RegExp(`data:image/${mediaFormat};base64,`);
      const base64Img = body.avatar.replace(re, '');
      await fsPromises.writeFile(newFilePath, base64Img, 'base64');

      newURL = path.join(avatarsDir_URL, newFileName);
    }

    return knex(USERS_TABLE)
      .update({
        username: body.username || null,
        email: body.email || null,
        avatar: newURL || null,
        age: body.age || null,
        phone: body.phone || null,
      })
      .where({ id });
  },

  async patchItem(id, body) {
    if (body.avatar) {
      const userObj = await knex(USERS_TABLE).select('*').where({ id }).first();
      if (userObj.avatar) {
        const oldFileName = path.basename(userObj.avatar);
        await fsPromises.unlink(path.join(avatarsDir, oldFileName));
      }
      const mediaFormat = body.avatar.substring(body.avatar.indexOf('/') + 1, body.avatar.indexOf(';'));
      const newFileName = Date.now().toString().concat(`.${mediaFormat}`);
      const newFilePath = path.join(avatarsDir, newFileName);

      const re = new RegExp(`data:image/${mediaFormat};base64,`);
      const base64Img = body.avatar.replace(re, '');

      await fsPromises.writeFile(newFilePath, base64Img, 'base64');

      const newURL = path.join(avatarsDir_URL, newFileName);
      body.avatar = newURL;
    }

    return knex(USERS_TABLE).update(body).where({ id });
  },

  async removeItem(id) {
    const userObj = await knex(USERS_TABLE).select('*').where({ id }).first();
    const oldFileName = path.basename(userObj.avatar);

    await fsPromises.unlink(path.join(avatarsDir, oldFileName));

    return knex(USERS_TABLE).where({ id }).del();
  },

  async getImg(filename) {
    const imagePath = path.join(avatarsDir, filename);

    const imgBuff = await fsPromises.readFile(imagePath);

    return imgBuff;
  },
};
